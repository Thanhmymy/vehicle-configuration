package co.simplon.vehicle.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.vehicle.Repository.IVehicleRepository;
import co.simplon.vehicle.entity.Vehicle;
import co.simplon.vehicle.factory.VehicleFactory;

@RestController
@RequestMapping(path = "/api/vehicle")
public class VehicleController {
    
    @Autowired
    IVehicleRepository vehicleRepository;

    @Autowired
    VehicleFactory vehicleFactory;

    @GetMapping("/{id}")
    public Vehicle findById(@PathVariable int id, @RequestParam String type){
        Vehicle vehicle = vehicleFactory.getVehicle(id,type);
        if (vehicle == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return vehicle;
    }  
}
