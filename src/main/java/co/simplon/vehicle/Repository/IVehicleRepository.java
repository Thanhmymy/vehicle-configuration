package co.simplon.vehicle.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.vehicle.entity.Vehicle;

@Repository
public interface IVehicleRepository extends JpaRepository<Vehicle, Integer>{
    
}
