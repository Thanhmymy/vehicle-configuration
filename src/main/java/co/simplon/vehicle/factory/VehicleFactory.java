package co.simplon.vehicle.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.simplon.vehicle.Repository.IVehicleRepository;
import co.simplon.vehicle.entity.Bicycle;
import co.simplon.vehicle.entity.Car;
import co.simplon.vehicle.entity.MotorBike;
import co.simplon.vehicle.entity.Vehicle;

@Component
public class VehicleFactory {
    
    @Autowired
    IVehicleRepository vehicleRepository;

    public Vehicle getVehicle(int id, String type){
        switch (type) {
            case "motorbike":
                MotorBike motorBike = (MotorBike)vehicleRepository.findById(id).get();
                return motorBike; 
            case "car":
            Car car = (Car)vehicleRepository.findById(id).get();
            return car;
            case "bicycle":
            Bicycle bicycle = (Bicycle)vehicleRepository.findById(id).get();
            return bicycle;
            default:
                return null;
        }
    }
}
