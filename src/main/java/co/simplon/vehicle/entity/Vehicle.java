package co.simplon.vehicle.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 80)
    private String brand;

    @Column(nullable = false, length = 30)
    private String color;

    @Column(nullable = false)
    private LocalDateTime buyingDate;

    @Column(nullable = false)
    private Float buyingPrice;

    @Column(nullable = false)
    private Float resalePrice;

    public enum vehicleType{
        MOTORBIKE, CAR, BICYCLE
    }

    @Column(columnDefinition = "ENUM('MOTORBIKE','BICYCLE','CAR')")
    @Enumerated(EnumType.STRING)
    private vehicleType vehicleType;

    public Vehicle( String brand,  String color,  LocalDateTime buyingDate,
            Float buyingPrice, Float resalePrice) {
        this.brand = brand;
        this.color = color;
        this.buyingDate = buyingDate;
        this.buyingPrice = buyingPrice;
        this.resalePrice = resalePrice;
    }

    public Vehicle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public LocalDateTime getBuyingDate() {
        return buyingDate;
    }

    public void setBuyingDate(LocalDateTime buyingDate) {
        this.buyingDate = buyingDate;
    }

    public Float getBuyingPrice() {
        return buyingPrice;
    }

    public void setBuyingPrice(Float buyingPrice) {
        this.buyingPrice = buyingPrice;
    }

    public Float getResalePrice() {
        return resalePrice;
    }

    public void setResalePrice(Float resalePrice) {
        this.resalePrice = resalePrice;
    }

}
