package co.simplon.vehicle.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@PrimaryKeyJoinColumn(name = "motorised_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class MotorBike extends Motorised {

    @Column(nullable = false, columnDefinition = "SMALLINT(4) UNSIGNED")
    private int cylinder;

    @Column(nullable = false, columnDefinition = "SMALLINT(4) UNSIGNED")
    private int power;


    public enum MotorBikeType {
        CROSS,ADVENTURE,NAKED,SCOOTER
    }

    @Column(columnDefinition = "ENUM('CROSS','ADVENTURE','NAKED','SCOOTER')")
    @Enumerated(EnumType.STRING)
    private MotorBikeType type;

    public int getCylinder() {
        return cylinder;
    }

    public void setCylinder(int cylinder) {
        this.cylinder = cylinder;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public MotorBikeType getType() {
        return type;
    }

    public void setType(MotorBikeType type) {
        this.type = type;
    }

    public MotorBike(int cylinder, int power, MotorBikeType type) {
        this.cylinder = cylinder;
        this.power = power;
        this.type = type;
    }

    public MotorBike() {
    }
}
