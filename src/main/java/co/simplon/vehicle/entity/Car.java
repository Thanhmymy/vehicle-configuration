package co.simplon.vehicle.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@PrimaryKeyJoinColumn(name = "motorised_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Car extends Motorised {

    public enum CarType {
        ELECTRIC, SUV, CROSSOVER, COUPE, CONVERTIBLE
    }

    @Column(columnDefinition = "ENUM('ELECTRIC','SUV','CROSSOVER','COUPE','CONVERTIBLE')")
    @Enumerated(EnumType.STRING)
    private CarType type;


    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    @Min(1)
    @Max(7)
    private int door;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean gearbox;

    @Column(nullable = false, columnDefinition = "SMALLINT(4)")
    private int power;


    public Car(CarType type, @Min(1) @Max(7) int door, Boolean gearbox, int power) {
        this.type = type;
        this.door = door;
        this.gearbox = gearbox;
        this.power = power;
    }

    public Car() {
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public int getDoor() {
        return door;
    }

    public void setDoor(int door) {
        this.door = door;
    }

    public Boolean getGearbox() {
        return gearbox;
    }

    public void setGearbox(Boolean gearbox) {
        this.gearbox = gearbox;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
