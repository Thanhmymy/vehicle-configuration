package co.simplon.vehicle.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@PrimaryKeyJoinColumn(name = "motorised_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Bicycle extends Vehicle {

    public enum BicycleType {
        BMX, CRUISER, ROAD, TOURING
    }

    @Column(columnDefinition = "ENUM('BMX','CRUISER','ROAD','TOURING')")
    @Enumerated(EnumType.STRING)
    private BicycleType type;

    
    @Column(nullable = false)
    private Boolean antiThief;

    @Column(nullable = false, columnDefinition = "TINYINT(1) UNSIGNED")
    private int gear;

    public Bicycle(BicycleType type, Boolean antiThief, int gear) {
        this.type = type;
        this.antiThief = antiThief;
        this.gear = gear;
    }

    public Bicycle() {
    }

    public BicycleType getType() {
        return type;
    }

    public void setType(BicycleType type) {
        this.type = type;
    }

    public Boolean getAntiThief() {
        return antiThief;
    }

    public void setAntiThief(Boolean antiThief) {
        this.antiThief = antiThief;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }
}
